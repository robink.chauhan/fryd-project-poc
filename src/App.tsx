import React, {
  MutableRefObject,
  Suspense,
  useEffect,
  useRef,
  useState,
} from "react";
import { useFrame, useLoader } from "@react-three/fiber";
import {
  ZapparCamera,
  ImageTracker,
  ZapparCanvas,
  Loader,
  BrowserCompatibility,
} from "@zappar/zappar-react-three-fiber";
import * as THREE from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

import glb from "./assets/fryd.glb";
import targetImage from "./assets/fryd-tracking-img.zpt";
import { Mesh } from "three";

// let action: THREE.AnimationAction;

const Model = () => {
  // const clock = new THREE.Clock();
  const gltf = useLoader(GLTFLoader, glb) as any;
  // const mixer = new THREE.AnimationMixer(gltf.scene);

  const { scene } = gltf;

  console.log("gltf", gltf.nodes.Cylinder);

  // action = mixer.clipAction(gltf.animations[0]);
  gltf.scene.rotateX(Math.PI / 2);

  useEffect(() => {
    const deg2rad = (deg: number) => {
      return deg * (Math.PI / 180.0);
    };
    if (gltf) {
      // let postitionZ = (window.innerWidth / 100) * 0.01;
      // let scale = (window.innerWidth / 100) * 0.28;
      gltf.scene.scale.set(0.09, 0.09, 0.09);
      gltf.scene.position.set(-0.5, 0.25, -0.2);
      gltf.scene.rotation.set(0, 0, 0);
      const { scene } = gltf;

      scene.children[0].children.forEach((element: any) => {
        element.scale.set(8, 8, 8);
      });
    }
  }, [gltf, gltf.scene.position, gltf.scene.rotation, gltf.scene.scale]);

  // return <primitive object={gltf.scene} />;

  const ref = useRef<Mesh>(null!);

  useEffect(() => {
    if (ref !== null) {
      // ref.current.position.z = -20;
    }
  }, []);

  useFrame((state, delta) => {
    if (ref.current.position.z <= 0.2284000000059605) {
      ref.current.position.z += delta;
    }
  });

  return (
    <mesh ref={ref}>
      <primitive object={gltf.scene} ref={ref}></primitive>
    </mesh>
  );
};

function App() {
  const [show, setShow] = useState(false);

  return (
    <>
      <BrowserCompatibility />
      <ZapparCanvas>
        <ZapparCamera />
        <Suspense fallback={null}>
          <ImageTracker
            onNotVisible={() => {
              setShow(false);
            }}
            onVisible={() => {
              setShow(true);
              // if (action) {
              //   action.play();
              // }
            }}
            targetImage={targetImage}
          >
            {show && (
              <React.Suspense fallback={null}>
                <Model />
              </React.Suspense>
            )}
          </ImageTracker>
        </Suspense>
        {/* <directionalLight position={[10, 0, 5]} intensity={1.2} />
        <directionalLight position={[10, 0, 5]} intensity={1.2} /> */}
        <spotLight position={[0, 0, 20]} intensity={1.2} />
        <Loader />
      </ZapparCanvas>
    </>
  );
}

export default App;
